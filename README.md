# SUPERNEWSLETTER DevStack

Devstack for easy newsletter creation.

## Getting Started

We're glad you chose to use our DevStack. There are a few simple things you need to know:

### Prerequisites

The things you need to install before running SUPERNEWSLETTER Devstack

```
Node	>= 8.9.4
NPM		>= 5.6.0
```

### Installing

It's easy – you need just one command :) It'll install dependencies and build templates with Browsersync and file watcher.

```
$ npm start
```

## How To Use

Command `$ npm run dev` builds templates into `dist` folder and starts virtual server at [http://localhost:3000/](http://localhost:3000/) with Browsersync and file watcher. Then any change in `src` folder recompiles templates and automatically refreshes your browser.

### Other NPM Commands

```
# command to build uncompressed templates
$ npm run dev

# command to build compressed templates
$ npm run build

# command to send templates to your e-mail
$ npm run mail
```

### Directory Structure

1. **src** - Source files folder
	1. **css** – [scss](https://sass-lang.com/)
	1. **img** – Images
	1. **tpl** – [Twig](https://twig.symfony.com/doc/2.x/)
	1. **fonts** – Webfonts *(not required)*
1. **tasks** - gulp tasks
1. **.editorconfig** - EditorConfig file
1. **.gitignore** - GIT ignore file
1. **.npmrc** - NPM config
1. **config.js** – DevStack config
1. **gulpfile.js** – Gulp config file
1. **package.json** - NPM dependencies

#### Generated Folders

1. **dist** - Folder with builded templates

## Author

* **SUPERKODERS** - [www.superkoders.com](https://superkoders.com/) – [tomas.krejci@superkoders.com](tomas.krejci@superkoders.com)

## License

The BSD 3-Clause License. Please see [LICENSE.md](LICENSE.md) for more information.
