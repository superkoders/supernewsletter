const { src, dest } = require("gulp");
const html2txt = require("gulp-html2txt");
const config = require("../config");

module.exports = function plaintext() {
	return src(["*.html"], {
		cwd: config.dist.tmp,
	})
		.pipe(
			html2txt({
				ignoreImage: true,
				wordwrap: 150,
				// singleNewLineParagraphs: true,
				// baseElement: true,
				noLinkBrackets: false,
				format: {
					heading: function (elem, fn, options) {
						var h = fn(elem.children, options);
						return "\n\n***" + h.toUpperCase() + "***\n\n";
					},
				},
			})
		)
		.pipe(dest(config.dist.txt));
};
