const gulp = require("gulp");
const notifier = require("node-notifier");
const browserSync = require("browser-sync");
const config = require("../config");
const copyimages = require("./copyimages");
const copyroot = require("./copyroot");
const sass = require("./sass");
const twig = require("./twig");
const inlinecss = require("./inlinecss");
const htmlbeautify = require("./htmlbeautify");

const { series, watch: watchGulp } = gulp;

module.exports = function watch(done) {
	return series(
		function syncing(syncDone) {
			browserSync(config.browserSync);
			syncDone();
		},
		function watching() {
			watchGulp(
				`${config.src.styles}**/*.scss`,
				series(gulp.parallel(sass, twig), inlinecss, htmlbeautify)
			);
			watchGulp(
				`${config.src.templates}**/*.twig`,
				gulp.series(twig, inlinecss, htmlbeautify)
			);
			watchGulp(`${config.src.images}**/*`, copyimages);
			watchGulp(
				[
					`${config.basePath.src}**/*`,
					`!${config.src.styles}**/*`,
					`!${config.src.images}**/*`,
					`!${config.src.templates}**/*`,
				],
				copyroot
			);

			notifier.notify({
				title: "SUPERKODERS NEWSLETTER stack",
				message: "Gulp is watching files.",
				sound: "Hero",
			});
		}
	)(done);
};
