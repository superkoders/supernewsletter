const { src, dest } = require("gulp");
const config = require("../config");

module.exports = function imagemin() {
	return import("gulp-imagemin").then((gulpImagemin) => {
		src(["**/*.{png,jpg,gif}"], {
			cwd: config.dist.images,
		})
			.pipe(
				gulpImagemin.default([
					gulpImagemin.mozjpeg({ progressive: true }),
					gulpImagemin.optipng(),
					gulpImagemin.gifsicle(),
				])
			)
			.pipe(dest(config.dist.images));
	});
};
