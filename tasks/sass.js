const { src, dest } = require("gulp");
const gulpSass = require("gulp-sass")(require("sass"));
const autoprefixer = require("autoprefixer");
const postcss = require("gulp-postcss");
const plumber = require("gulp-plumber");
const config = require("../config");
const logger = require("./helpers/logger.js");

module.exports = function sass(done) {
	const settings = {
		includePaths: ["bower_components", "node_modules"],
		outputStyle: "expanded",
		precision: 10,
	};

	return src(["*.scss"], {
		cwd: config.src.styles,
	})
		.pipe(
			plumber({
				errorHandler: logger.onError({
					title: "Sass error!",
					callback: done,
				}),
			})
		)

		.pipe(gulpSass.sync(settings))
		.pipe(
			postcss([
				autoprefixer({
					grid: "autoplace",
				}),
			])
		)
		.pipe(dest(config.dist.styles));
};
