const { src, dest } = require("gulp");
const config = require("../config");

module.exports = function copyimages() {
	return src(["**/*.{png,jpg,gif}"], {
		cwd: config.src.images,
	}).pipe(dest(config.dist.images));
};
