const { src, dest } = require("gulp");
const config = require("../config.js");

module.exports = function copyroot() {
	return src(
		["**/*", "!css/**/*", "!img/**/*", "!tpl/**/*", "!css", "!img", "!tpl"],
		{
			cwd: config.basePath.src,
			dot: true,
		}
	).pipe(dest(config.basePath.dist));
};
