const { src } = require("gulp");
const gulpMail = require("gulp-mail");
const config = require("../config");

const smtpInfo = {
	auth: {
		user: "skoleni-emailu@seznam.cz",
		pass: "CpxurNt7Rq5sDx8",
	},
	host: "smtp.seznam.cz",
	secureConnection: true,
	port: 465,
};

module.exports = function mail() {
	return src(["*.html"], {
		cwd: config.dist.templates,
	}).pipe(
		gulpMail({
			subject: "SuperTest",
			to: ["martin.frystak@superkoders.com"],
			from: "Školení e-mailů <skoleni-emailu@seznam.cz>",
			smtp: smtpInfo,
		})
	);
};
