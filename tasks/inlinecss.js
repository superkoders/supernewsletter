const { src, dest } = require("gulp");
const inlineCss = require("gulp-inline-css");
const config = require("../config");

module.exports = function inlinecss() {
	return src(["*.html"], {
		cwd: config.dist.templates,
	})
		.pipe(
			inlineCss({
				applyStyleTags: false,
				applyLinkTags: true,
				removeStyleTags: false,
				removeLinkTags: true,
			})
		)
		.pipe(dest(config.dist.tmp));
};
