const basePath = {
	src: "src/",
	dist: "dist/",
};

const src = {
	images: basePath.src + "img/",
	styles: basePath.src + "css/",
	templates: basePath.src + "tpl/",
	components: basePath.src + "tpl/components/",
	layout: basePath.src + "tpl/layout/",
};

const twigNamespaces = {
	components: src.components,
	layout: src.layout,
	images: src.images,
	templates: src.templates,
};

const dist = {
	images: basePath.dist + "img/",
	styles: basePath.dist + "css/",
	templates: basePath.dist + "tpl/",
	tmp: basePath.dist + "tmp/",
	txt: basePath.dist + "txt/",
};

const browserSync = {
	open: false,
	notify: false,
	reloadThrottle: 1000,
	watch: true,
	server: {
		baseDir: basePath.dist,
	},
};

module.exports = {
	basePath,
	src,
	twigNamespaces,
	dist,
	browserSync,
};
