const { series, parallel } = require("gulp");

const mailTask = require("./tasks/mail");

const cleanTask = require("./tasks/clean");
const sassTask = require("./tasks/sass");
const twigTask = require("./tasks/twig");
const copyrootTask = require("./tasks/copyroot");
const copyimagesTask = require("./tasks/copyimages");
const imageminTask = require("./tasks/imagemin");
const inlinecssTask = require("./tasks/inlinecss");
const htmlbeautifyTask = require("./tasks/htmlbeautify");
const plaintextTask = require("./tasks/plaintext");
const watch = require("./tasks/watch");

const build = function build(done) {
	return series(
		cleanTask,
		parallel(sassTask, twigTask, copyrootTask, copyimagesTask),
		inlinecssTask,
		htmlbeautifyTask
	)(done);
};

const dev = function dev(done) {
	return series(build, watch)(done);
};

const min = function min(done) {
	return series(build, parallel(imageminTask, plaintextTask))(done);
	// return series(build, imageminTask)(done);
};

const mail = function mail(done) {
	return series(mailTask)(done);
};

const clean = function clean(done) {
	return series(cleanTask)(done);
};
const sass = function sass(done) {
	return series(sassTask)(done);
};
const twig = function twig(done) {
	return series(twigTask)(done);
};
const copyroot = function copyroot(done) {
	return series(copyrootTask)(done);
};
const copyimages = function copyimages(done) {
	return series(copyimagesTask)(done);
};
const inlinecss = function inlinecss(done) {
	return series(inlinecssTask)(done);
};
const htmlbeautify = function htmlbeautify(done) {
	return series(htmlbeautifyTask)(done);
};

module.exports = {
	default: dev,
	min,
	mail,
	clean,
	sass,
	twig,
	copyroot,
	copyimages,
	inlinecss,
	htmlbeautify,
};
